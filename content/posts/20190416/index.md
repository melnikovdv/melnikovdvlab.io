---
title: "Instagram-блиц 🎯"
date: 2019-04-16T01:37:16+03:00
tags: ["soft"]
---

1. В какой стране наименьшее количество лиц на фотографиях?
2. У кого больше всего подписчиков в Instagram?
3. В какой стране Instagram-пользователей больше, чем Facebook-пользователей?
4. Почему на ближнем востоке гораздо больше мужских аккаунтов, нежели женских?

<!--more-->

А теперь ответы:

1) Япония. Там довольно серьезно к privacy относятся, и люди должны спрашивать у тех, кто попал в кадр, можно ли публиковать фото с ними. 

2) Долгое время лидером была певица Селена Гомез, но сейчас первую строчку занимает Криштиану Рональдо. У обоих под 150 млн подписчиков. 

3) В России. Потому что Vk и Ok победили Fb. 

Предвосхищаю возгласы про Китай, но там и Instagram, и Facebook много лет как заблокированы. Также вы там не найдете без VPN'ов и ухищерений Google, Twitter, YouTube, Netflix и прочих.

4) Потому что на востоке у женщин меньше прав и они боятся атак со стороны их общества. 

В топе Instagram в основном футболисты и музыканты (pop-stars). И, если в музыкантах я не сомневался, всенародная увлеченность футболом меня поражает.

https://en.wikipedia.org/wiki/List_of_most-followed_Instagram_accounts

Взято из выступления George Lee (ex Head of Growth in Instagram, now Derictor Product Management in Facebook). Видео не самое увлекательное, однако местами было любопытно послушать.  

{{< youtube 3ksSTWSheRo >}}

