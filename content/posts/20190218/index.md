---
title: "NBA All-Star Weekend 2019"
date: 2019-02-18T01:55:36+03:00
tags: ["blog"]
---

Вы, наверно, не слышали, но за океаном есть баскетбол. И вот, в выходные прошел очередной All-Star Weekend, где, помимо матча всех звезд, также проходили развлекухи типа 3 Point contest, Skills challenge и Dunk Contest.

Посмотите на лучший бросок сверху в этому году: Hamidou Diallo (1м 96 см) перепрыгивает Shaquille O'Neal (2м 16см, 150кг) и по локоть уходит в кольцо (3м 5см). Wow!

{{< youtube oim-sHtqfg4 >}}

Дети, не повторяйте это дома! Хотя у вас и не получится.