---
title: "Hellboy 😈"
date: 2019-04-14T01:44:16+03:00
tags: ["movie"]
---

Hellboy ‒ рейтинг R (18+) и очень низкие оценки на IMDB и rottentomatoes.

<!--more-->

- сказка стоимостью 50 млн долларов
- 2 часа трэша с кровищей и кишками
- прекрасная картинка; демон с рогами великолепен
- аутентичная баба Яга в избушке на курьих ножках (Neil Marshall, оказывается, поклонник русского фольклора; а еще он был режиссером эпизодов в Game of Thrones и Westworld)
- отменный юмор
- крутой саундтрек

{{< youtube ZsBO4b3tyZg >}}

