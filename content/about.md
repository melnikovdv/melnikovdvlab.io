---
title: "About"
---

<img src="/images/photo.jpg" width="150">

I'm Dmitry Melnikov. I write code and I like it 😎. My main areas of interest are web and Android development.  

Besides tech I play tennis and fond of swing dances. 

Follow me in [Instagram][1], [Fb][2], [Vk][3], [Telegram][4], [Tamtam][5].

[1]: https://www.instagram.com/solvadore/
[2]: https://www.facebook.com/melnikovdv
[3]: https://vk.com/melnikovdv
[4]: https://t.me/melnikovdv
[5]: https://tt.me/melnikovd